package balance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CsvReader {

	public static final boolean DEV_MODE = false;

	private static final Logger LOGGER = Logger.getLogger(CsvReader.class.getName());

	private static final String UNEXPECTEDPATTERN = "sms;deliver;\\\"KREDOBANK\\\";\\\"\\\";\\\"\\\";\\\"[\\\\d\\\\.: ]+\\\";\\\"\\\";\\\"(\\\\d{2}\\\\.\\\\d{2}\\\\.\\\\d{4} \\\\d{2}:\\\\d{2})";
	private static final String EXPENSEPATTERN = "sms;deliver;\"KREDOBANK\";\"\";\"\";\"[\\d\\.: ]+\";\"\";\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) KUPIVLIA .*?(\\d+\\.\\d{2})UAH CARD.*ZALISHOK (\\d+\\.\\d{2}) UAH.*";
	private static final String INCOMEPATTERN =  "sms;deliver;\"KREDOBANK\";\"\";\"\";\"[\\d\\.: ]+\";\"\";\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) ZARAKHUVANIA (\\d+\\.\\d{2})UAH CARD.*ZALISHOK (\\d+\\.\\d{2}) UAH.*";
	private static final String TRANSFETPATTERN = "sms;deliver;\\\"KREDOBANK\\\";\\\"\\\";\\\"\\\";\\\"[\\d\\.: ]+\\\";\\\"\\\";\\\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) KREDOBANK PEREKAZ (\\d+\\.\\d{2})UAH .*ZALISHOK (\\d+\\.\\d{2}) UAH.*";
	private static final String WITHDRAWPATTERN = "sms;deliver;\\\"KREDOBANK\\\";\\\"\\\";\\\"\\\";\\\"[\\d\\.: ]+\\\";\\\"\\\";\\\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) KREDOBANK ZNYATIA HOTIVKY (\\d+\\.\\d{2})UAH .*ZALISHOK (\\d+\\.\\d{2}) UAH.*";
	private static final String EXPENSEININTERNETPATTERN = "sms;deliver;\"KREDOBANK\";\"\";\"\";\"[\\d\\.: ]+\";\"\";\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) INTERNET .*?(\\d+\\.\\d{2})UAH .*ZALISHOK (\\d+\\.\\d{2}) UAH.*";
	private static final String WRITEOFF = "sms;deliver;\"KREDOBANK\";\"\";\"\";\"[\\d\\.: ]+\";\"\";\"(\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}) KREDOBANK SPYSANIA .*?(\\d+\\.\\d{2})UAH CARD.*ZALISHOK (\\d+\\.\\d{2}) UAH.*\"";
	
	private List<LocalDateTime> dates = null;
	private List<Float> amounts = null;
	private List<Float> balances = null;

	public void readFile(File file ) throws IOException {
		LOGGER.addHandler(new ConsoleHandler());
		Pattern unexpectedPatternPattern = Pattern.compile(UNEXPECTEDPATTERN);

		List<Pattern> incomes = new ArrayList<>(1);
		incomes.add(Pattern.compile(INCOMEPATTERN));
		Function<String, Matcher> getIncomesMatcherFn = getMatcher(incomes);

		List<Pattern> expenses = new ArrayList<>(5);
		expenses.add(Pattern.compile(EXPENSEPATTERN));
		expenses.add(Pattern.compile(TRANSFETPATTERN));
		expenses.add(Pattern.compile(WITHDRAWPATTERN));
		expenses.add(Pattern.compile(EXPENSEININTERNETPATTERN));
		expenses.add(Pattern.compile(WRITEOFF));
		Function<String, Matcher> getExpensesMatcherFn = getMatcher(expenses);

		dates = new ArrayList<>();
		amounts = new ArrayList<>();
		balances = new ArrayList<>();

		try (BufferedReader inputStream = new BufferedReader(new FileReader(file))) {
			String l;
			Matcher matcher = null;
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
			while ((l = inputStream.readLine()) != null) {
				String sign = "-";

				matcher = getIncomesMatcherFn.apply(l);
				if (matcher != null) {
					sign = "";
				}

				if (matcher == null) {
					matcher = getExpensesMatcherFn.apply(l);
				}

				if (matcher == null) {
					if (unexpectedPatternPattern.matcher(l).find()) {
						LOGGER.warning("Suspitions pattern of SMS found!");
						LOGGER.warning(l);
					}
				}
				else {
					matcher.reset(); // reset after the previous find()
					while (matcher.find()){
						dates.add(LocalDateTime.parse(matcher.group(1), formatter));
						amounts.add(Float.parseFloat(sign + matcher.group(2)));
						balances.add(Float.parseFloat(matcher.group(3)));
					}
					logMatchedLine(l);
				}
			}
		}
	}

	private void logMatchedLine(String l) throws IOException {
		if (DEV_MODE) {
			try(BufferedWriter o = new BufferedWriter(new FileWriter("log.txt", true))){
				o.write(l);
				o.newLine();
			}
		}
	}

	public List<LocalDateTime> getDates() {
		return dates;
	}

	public List<Float> getAmounts() {
		return amounts;
	}

	public List<Float> getBalances() {
		return balances;
	}

	private Function<String, Matcher> getMatcher(List<Pattern> patterns){
		return l -> {
			for(Pattern pat : patterns) {
				Matcher m = pat.matcher(l);
				if(m.find()) {
					return m;
				}
			}
			return null;
		};
	}
}
