package balance;

import java.time.LocalDateTime;

public class Entity {

	private LocalDateTime date;
	private Float income;
	private Float expense;
	private Float balance;

	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public Float getIncome() {
		return income;
	}
	public void setIncome(Float income) {
		this.income = income;
	}
	public Float getExpense() {
		return expense;
	}
	public void setExpense(Float expense) {
		this.expense = expense;
	}
	public Float getBalance() {
		return balance;
	}
	public void setBalance(Float balance) {
		this.balance = balance;
	}
	public Entity date(LocalDateTime date) {
		setDate(date);
		return this;
	}
	public Entity income(Float income) {
		setIncome(income);
		return this;
	}
	public Entity expense(Float expense) {
		setExpense(expense);
		return this;
	}
	public Entity balance(Float balance) {
		setBalance(balance);
		return this;
	}
}
