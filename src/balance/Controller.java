package balance;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class Controller {

	private Repository repository;

	public Controller(Repository repository) {
		this.repository = repository;
	}

	public void connectToDb() throws Exception {
		repository.connect();
	}
	public void disconnectToDb() throws Exception {
		repository.shutdown();
	}
	
	public int loadSmsDump(File file) throws IOException {
		CsvReader csvReader = new CsvReader();
		csvReader.readFile(file);

		List<LocalDateTime> dates = csvReader.getDates();
		List<Float> amounts = csvReader.getAmounts();
		List<Float> balances = csvReader.getBalances();
		
		List<Entity> entities = new ArrayList<>(dates.size());
		Iterator<LocalDateTime> datesIterator = dates.iterator();
		Iterator<Float> amountsIterator = amounts.iterator();
		Iterator<Float> balancesIterator = balances.iterator();

		while(datesIterator.hasNext()) {
			entities.add(new Entity()
								.date(datesIterator.next())
								.income(amountsIterator.next())
								.balance(balancesIterator.next())
								);
		}

		repository.update(entities);
		return entities.size();
	}

	public Float[][] getByMonths(List<String> columnKeys) {
		List<Entity> entities = repository.getEntitiesByMonth();
		fillColumnKeysWithYearMonth(entities, columnKeys);
		return convertEntitesToArray(entities);
	}

	public Float[][] getByDays(List<String> columnKeys) {
		List<Entity> entities = repository.getEntitiesByDay();
		fillColumnKeysWithYearDay(entities, columnKeys);
		return convertEntitesToArray(entities);
	}

	private void fillColumnKeysWithYearMonth(List<Entity> entities, List<String> columnKeys) {
		columnKeys.clear();
		for (Entity en : entities) {
			LocalDateTime datetime = en.getDate();
			String month = datetime.getMonth().getDisplayName(TextStyle.SHORT, Locale.UK);
			String year = String.valueOf(datetime.getYear());
			columnKeys.add(month + "'" + year.substring(2));
		}
	}

	private void fillColumnKeysWithYearDay(List<Entity> entities, List<String> columnKeys) {
		columnKeys.clear();
		for (Entity en : entities) {
			LocalDateTime datetime = en.getDate();
			String month = datetime.getMonth().getDisplayName(TextStyle.SHORT, Locale.UK);
			String year = String.valueOf(datetime.getYear());
			String day = String.valueOf(datetime.getDayOfMonth());
			columnKeys.add(day + "'" + month + "'" + year.substring(2));
		}
	}

	private Float[][] convertEntitesToArray(List<Entity> entities) {
		Float[][] res = new Float[3][entities.size()];
		for (int idx = 0; idx < entities.size(); ++idx) {
			Entity en = entities.get(idx);
			res[0][idx] = en.getIncome();
			res[1][idx] = en.getExpense();
			res[2][idx] = en.getBalance();
		}
		return res;
	}
}
