package balance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

public class Repository {

	private static final Logger LOGGER = Logger.getLogger(Repository.class.getName());

	private static final String TABLENAME = "balance";
	private static final String DBNAME="derby/jdbcBalanceDB";
	private static final String CONNECTIONURL = "jdbc:derby:" + DBNAME + ";create=true";
	private Connection conn;

	public void update(List<Entity> entities) {
		for (Entity en : entities) {
			String query = "INSERT INTO " + TABLENAME + " VALUES (?, ?, ?)";
			try (PreparedStatement psInsert = conn.prepareStatement(query)){
				psInsert.setTimestamp(1, java.sql.Timestamp.valueOf(en.getDate()));
				psInsert.setFloat(2, en.getIncome());
				psInsert.setFloat(3, en.getBalance());
				psInsert.executeUpdate();
				// the statement will be closed automatically by try-with-resources
			} catch (SQLException sqle) {
				if (sqle.getSQLState().equals("23505")) {
					LOGGER.info("This date have already been added.");
				}
				else {
					LOGGER.warning(String.format("%d - %s", sqle.getErrorCode(), sqle.getMessage()));
				}
			}
		}
	}

	private List<Entity> getEntities(DateKey dk) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
		SortedMap<Integer, Entity> mapRes = new TreeMap<>(); /*Key - number of month */
		try {
			String query = 
					"SELECT"
						+ " CAST(YEAR(date) AS CHAR(4)) AS y,"
						+ " CAST(MONTH(date) AS CHAR(2)) AS m,"
						+ " DAY(date) AS d,"
						+ " date AS full_date,"
						+ " CAST(income AS DECIMAL(9,2)) AS income,"
						+ " CAST(balance AS DECIMAL(9,2)) AS balance"
					+ " FROM balance"
					+ " ORDER BY date";
			try (Statement s = conn.createStatement()) {
				try (ResultSet res = s.executeQuery(query)) {
					while (res.next()){
						Integer datekey = dk.convertResToDateKey(res);
						Entity en = mapRes.get(datekey);
						Float income = res.getFloat("income");
						LocalDateTime fullDate = LocalDateTime.parse(res.getString("full_date"), formatter);
						if (en == null) {
							en = new Entity().date(fullDate)
											.income(income > 0 ? income : (float)0.0)
											.expense(income < 0 ? Math.abs(income) : (float)0.0)
											.balance(res.getFloat("balance")); //NOSONAR
						}
						else {
							en.setDate(fullDate);
							if (income > 0) {
								en.setIncome(en.getIncome() + income);
							}
							else {
								en.setExpense(en.getExpense() + Math.abs(income));
							}
							en.setBalance(res.getFloat("balance")); //NOSONAR
						}
						mapRes.put(datekey, en);
					}
				}
			}
		}
		catch (SQLException sqle) {
			LOGGER.warning(String.format("%d - %s", sqle.getErrorCode(), sqle.getMessage()));
		}

		return new ArrayList<>(mapRes.values());
	}

	public List<Entity> getEntitiesByMonth() {
		DateKey dk = res -> res.getInt("y") * 100 + res.getInt("m");
		return getEntities(dk);
	}

	public List<Entity> getEntitiesByDay() {
		DateKey dk = res -> res.getInt("y") * 10000 + res.getInt("m") * 100 + res.getInt("d");
		return getEntities(dk);
	}

	public void connect() throws RepositoryException {
		try {
			conn = DriverManager.getConnection(CONNECTIONURL);
			makeSureTheTableExists(conn);
		} catch (SQLException e) {
			String cause = e.getMessage();
			throw new RepositoryException(cause);
		}
	}
	
	public void shutdown() throws RepositoryException {
		/* Shutdown throws the XJ015 exception to confirm success. */
		try {
			conn.close();
			DriverManager.getConnection("jdbc:derby:;shutdown=true");
		}
		catch (SQLException sqle) {
			if (!sqle.getSQLState().equals("XJ015") ) {
				throw new RepositoryException(sqle.getMessage());
			}
		}
	}

	public static void makeSureTheTableExists(Connection conn) throws SQLException {
		try (Statement s = conn.createStatement()) {
			s.execute("CREATE TABLE " + TABLENAME 
					+" (date TIMESTAMP NOT NULL, "
					+ "income FLOAT NOT NULL, "
					+ "balance FLOAT NOT NULL, "
					+ "PRIMARY KEY (date, income, balance))");
		}
		catch (SQLException sqle) {
			String theError = (sqle).getSQLState();
			if (!theError.equals("X0Y32")) { // Table/View already exists in Schema 'APP'.
				throw sqle; 
			}
		}
	}
}


class RepositoryException extends Exception {

	public RepositoryException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}


@FunctionalInterface
interface DateKey {
	Integer convertResToDateKey(ResultSet res) throws SQLException;
}
