package balance;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LayeredBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.JMenu;
import javax.swing.JFileChooser;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JSlider;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

public class Balance {

	private static final Logger LOGGER = Logger.getLogger(Balance.class.getName());
	private static final int RANGE = 12;

	private JFrame frmTheGraph;
	private Controller controller;
	final List<String> columnKeys = new ArrayList<>();
	Float[][] data = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				Repository repository = new Repository();
				Balance window = new Balance(new Controller(repository));
				window.frmTheGraph.setVisible(true);
			} catch (Exception e) {
				LOGGER.warning(String.format("%s - %s", e.getCause(), e.getMessage()));
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Balance(Controller controller) {
		this.controller = controller;
		try {
			initialize();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 * @wbp.parser.entryPoint
	 */
	private void initialize() throws Exception {
		controller.connectToDb();

		frmTheGraph = new JFrame();
		frmTheGraph.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				try {
					controller.disconnectToDb();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
		frmTheGraph.setTitle("Balance");
		frmTheGraph.setBounds(200, 200, 900, 600);
		frmTheGraph.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		frmTheGraph.getContentPane().setLayout(gridBagLayout);

		data = controller.getByMonths(columnKeys);
		final CategoryDataset dataset = createDatasetRange(0, RANGE);
		final JFreeChart chart = createChart(dataset);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(1000, 540));
		GridBagConstraints gbc_chart = new GridBagConstraints();
		gbc_chart.gridheight = 7;
		gbc_chart.insets = new Insets(5, 5, 5, 0);
		gbc_chart.fill = GridBagConstraints.BOTH;
		gbc_chart.gridwidth = 2;
		gbc_chart.gridx = 0;
		gbc_chart.gridy = 0;
		frmTheGraph.getContentPane().add(chartPanel, gbc_chart);

		JMenuBar menuBar = new JMenuBar();
		frmTheGraph.setJMenuBar(menuBar);

		JMenu mnLoad = new JMenu("Load");
		mnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me) {
				// Create a file chooser
				JFileChooser fc = new JFileChooser();
				// In response to a button click:
				int returnVal = fc.showOpenDialog(frmTheGraph);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					JProgressBar progressBar = new JProgressBar(0, 100);
					progressBar.setValue(0);
					progressBar.setStringPainted(true);
					progressBar.setString("Loading...");
					progressBar.setIndeterminate(true);
					frmTheGraph.getContentPane().add(progressBar, gbc_chart);
					Runnable task = () -> {
						try {
							chartPanel.setVisible(false);
							progressBar.setVisible(true);
							int size = controller.loadSmsDump(file);
							progressBar.setVisible(false);
							chartPanel.setVisible(true);
							JOptionPane.showMessageDialog(null, String.format("%d proccessed.", size));
						} catch (IOException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					};
					
					Thread thread = new Thread(task);
					thread.start();
				}
			}
		});
		menuBar.add(mnLoad);
		
		JSlider slider = new JSlider();
		slider.setValue(RANGE);
		slider.setMinimum(RANGE);
		slider.setMaximum(columnKeys.size());
		slider.addChangeListener(e -> {
			JSlider source = (JSlider)e.getSource();
			slider.setMaximum(columnKeys.size());
			int r = source.getValue();
			chart.getCategoryPlot().setDataset(createDatasetRange(r-RANGE, r));
		});
		slider.setToolTipText("slide");
		slider.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider.weightx = 10.0;
		gbc_slider.gridwidth = 2;
		gbc_slider.insets = new Insets(0, 0, 5, 0);
		gbc_slider.gridx = 0;
		gbc_slider.gridy = 7;
		frmTheGraph.getContentPane().add(slider, gbc_slider);
		
		JButton btnDays = new JButton("days");
		btnDays.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				data = controller.getByDays(columnKeys);
				slider.setMaximum(columnKeys.size());
				chart.getCategoryPlot().setDataset(createDatasetRange(0, RANGE));
				chart.getCategoryPlot().getDomainAxis().setLabel("Days");
			}
		});
		
		JButton btnNewButton = new JButton("months");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				data = controller.getByMonths(columnKeys);
				slider.setMaximum(columnKeys.size());
				chart.getCategoryPlot().setDataset(createDatasetRange(0, RANGE));
				chart.getCategoryPlot().getDomainAxis().setLabel("Months");
			}
		});
		btnNewButton.setToolTipText("months");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 8;
		frmTheGraph.getContentPane().add(btnNewButton, gbc_btnNewButton);
		btnDays.setToolTipText("days");
		GridBagConstraints gbc_btnDays = new GridBagConstraints();
		gbc_btnDays.insets = new Insets(0, 0, 5, 0);
		gbc_btnDays.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDays.gridx = 1;
		gbc_btnDays.gridy = 8;
		frmTheGraph.getContentPane().add(btnDays, gbc_btnDays);
	}

	private CategoryDataset createDataset(List<String> columnKeys, Float[][] data) {
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (int idx = 0; idx < columnKeys.size(); ++idx) {
			String colKey = columnKeys.get(idx);
			dataset.addValue(data[0][idx], "incomes", colKey);
			dataset.addValue(data[1][idx], "expenses", colKey);
			dataset.addValue(data[2][idx], "balances", colKey);
		}
		return dataset;
	}

	private CategoryDataset createDatasetRange(int start, int end) {
		Float[][] d;
		List<String> cKeys;
		if (columnKeys.size() > end-start) {
			d = new Float[3][end-start];
			cKeys = columnKeys.subList(start, end);
			for(int idx = start, jdx = 0; idx < end; ++idx, ++jdx) {
				d[0][jdx] = data[0][idx];
				d[1][jdx] = data[1][idx];
				d[2][jdx] = data[2][idx];
			}
		}
		else {
			d = data;
			cKeys = columnKeys;
		}
		return createDataset(cKeys, d);
	}

	private JFreeChart createChart(final CategoryDataset dataset) {

		final CategoryAxis categoryAxis = new CategoryAxis("Months");
		final ValueAxis valueAxis = new NumberAxis("Money (₴)");

		final CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, new LayeredBarRenderer());

		plot.setOrientation(PlotOrientation.VERTICAL);
		final JFreeChart chart = new JFreeChart("Balance", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		// set the background color for the chart...
		chart.setBackgroundPaint(Color.lightGray);

		final LayeredBarRenderer renderer = (LayeredBarRenderer) plot.getRenderer();

		// we can set each series bar width individually or let the renderer manage a
		// standard view.
		// the width is set in percentage, where 1.0 is the maximum (100%).
		renderer.setSeriesBarWidth(0, 1.0);
		renderer.setSeriesBarWidth(1, 0.6);
		renderer.setSeriesBarWidth(2, 0.3);
		renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		renderer.setItemMargin(0.01);
		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryMargin(0.25);
		domainAxis.setUpperMargin(0.05);
		domainAxis.setLowerMargin(0.05);

		return chart;
	}
}
